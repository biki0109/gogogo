package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogogo/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type GogogoClient struct {
	pb.GogogoClient
}

func NewGogogoClient(address string) *GogogoClient {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogogo service", l.Error(err))
	}

	c := pb.NewGogogoClient(conn)

	return &GogogoClient{c}
}
