// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/gogogo/internal/services"
	"git.begroup.team/platform-transport/gogogo/internal/stores"
	"git.begroup.team/platform-transport/gogogo/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.GogogoServer {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
