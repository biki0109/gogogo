package services

import (
	"context"

	"git.begroup.team/platform-transport/gogogo/internal/stores"
	"git.begroup.team/platform-transport/gogogo/pb"
	"honnef.co/go/tools/config"
)

const Version = "1.0.0"

type service struct {
	isReady   bool
	cfg       *config.Config
	mainStore *stores.MainStore
}

func New(config *config.Config,
	mainStore *stores.MainStore) pb.GogogoServer {
	return &service{
		isReady:   true,
		cfg:       config,
		mainStore: mainStore,
	}
}

func (s *service) Version(context context.Context, req *pb.VersionRequest) (*pb.VersionResponse, error) {
	return &pb.VersionResponse{
		Version: Version,
	}, nil
}
